'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('person', {
      id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      email: {
        type: Sequelize.STRING(50),
        allowNull: false,
        unique: true
      },
      name: {
        type: Sequelize.STRING(50),
        defaultValue: ''
      },
      surname: {
        type: Sequelize.STRING(50),
        defaultValue: ''
      },
      age: {
        type: Sequelize.INTEGER(11),
        defaultValue: '',
        allowNull: false,
        defaultValue: '0'
      },
      gender: {
        type: Sequelize.STRING(10),
        defaultValue: ''
      },
      birthday: {
        type: Sequelize.STRING(50),
      },
      phone: {
        type: Sequelize.STRING(50),
        defaultValue: ''
      },
      createdAt: Sequelize.DATE,
      modified: Sequelize.DATE
      
    }, {
      freezeTableName: true
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('person')
  }
}
