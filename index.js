const express = require('express')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const morgan = require('morgan')
const db = require('./src/models')
const logger = require('./winston-config')
const routes = require('./src/routes')
const xss = require('xss-clean')
const rateLimit = require('express-rate-limit')
const hpp = require('hpp')
require('dotenv').config()
logger.info(process.env.NODE_PORT)
let port = process.env.NODE_PORT
if (isNaN(parseInt(port))) {
  port = 3000
}
// create express app
const app = express()

// Middlewares
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(helmet())

if (process.env.NODE_ENV !== 'production') {
  // app.use(morgan('dev'));
  app.use(
    morgan('dev', {
      stream: logger.stream
      // only log error responses
      // skip: (req, res) => {
      //     return res.statusCode < 400;
      // },
    })
  )
}

// Set security headers
app.use(helmet())

// Prevent XSS attacks
app.use(xss());

// Prevent http param pollution
app.use(hpp());


// Rate limiting
const limiter = rateLimit({
  windowMs: 10 * 60 * 1000, // 10 mins
  max: 100
});
app.use(limiter);


db.sequelize.sync()
  .then(() => {
    logger.info('We are connected to the database')
  })
  .catch((err) => {
    console.error(`Unable to connect to the database:' ${err.stack}`)
    throw new Error(`Unable to connect to the database:' ${err.message}`)
  })

app.use('/api', routes)

// throw 404 if URL not found
app.all("*", function(req, res,next) {
  logger.error('Resource not found')
    res.status(404).json({
      success: false,
      error: 'Resource not found'
    });
});

const server = app.listen(port, () => {
  logger.info(`server started on port ${port}`)
})

process.on('SIGINT', () => {
  logger.warn('SIGINT RECEIVED. Shutting down gracefully')
  server.close(() => {
    logger.info('💥 Process terminated!')
  })
})

module.exports = server;
