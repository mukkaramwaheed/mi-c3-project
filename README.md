# Introduction
mi-c3-project save the person and contacts detail. Person detail store in people table and contacts data saved in contacts table. Foreign key of person store in contacts table. Each contact linked with some person,
email may be duplicate in contacts table. Only person CRUD implemented.

# Important

Person table

Only single contact created in contacts table against one person and contact not be array it must be object and it's optional. Contact object only used in the person create request (/api/person/create)

- Sample data

```
{
    "email": "mukkaramwaheed@gmail.com",
    "name": "mukkaramwaheed",
    "surname": "mukkaram",
    "gender": "male",
    "phone": "00923334746636",
    contact: {
        "email": "adam@gmail.com",
        "name": "nik",
        "surname": "adam",
        "gender": "male",
        "phone": "0035627036156",
    }
    
}

```

# Validation rules

Person table validation and required fields

- email must be unique and valid email
- name
- surname

Optional

- gender must be male or female
- phone only contains number without special character and space between them
- age numeric value only and it must be valid age according to birthday
- birthday date format (YYYY-MM-DD)

Contact table validation and required fields

Optional

- email must be valid email
- name
- surname
- gender must be male or female
- phone only contains number without special character and space between them
- age numeric value only and it must be valid age according to birthday


# Implementation
Main component that can be found in `src/index.js`.

# Usage

Rename the .env.example to .env and setup the database detail

```
npm install
```

```
nodemon serve
```

# Routes

Create person method POST
```
{{URL}}/api/person/create

Sample data

{
    "email": "john@gmail.com",
    "name": "john",
    "surname": "don",
    "gender": "male",
    "phone": "0035627036155",
     contact: {
        "email": "adam@gmail.com",
        "name": "nik",
        "surname": "adam",
        "gender": "male",
        "phone": "0035627036156",
    }
    
}
```
Update person method PATCH
```
{{URL}}/api/person/update/{id}
```

Get person by id method GET
```
{{URL}}/api/person/get/{id}
```

Delete person by id method DELETE
```
{{URL}}/api/person/delete/{id}
```

Get all people method GET, default limit is 10
```
{{URL}}/api/person?limit=10&page=1
```

## Testing

Make sure db and tables created

 - Functional testing using Mocha
```
npm run test
```
