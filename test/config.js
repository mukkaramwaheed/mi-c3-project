const chai = require('chai');
const chaiHttp = require('chai-http');
// const should = chai.should();
const request = require('supertest');
const expect = require('chai').expect;
const server = require('../index');
const faker = require('faker');
chai.use(chaiHttp)

module.exports = {
    // chai,
    server,
    expect,
    request,
    faker
}