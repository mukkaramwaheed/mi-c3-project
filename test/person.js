const { server, expect, request, faker } = require('./config')
var personId
var email = faker.internet.email()

describe('Create new person', function () {
  it('should respond with person object', function (done) {
    request(server)
      .post('/api/person/create')
      .send({
        "email": email,
        "name": faker.name.firstName(),
        "surname": faker.name.lastName(),
        "gender": 'male',
        "phone": faker.phone.phoneNumberFormat().replace(/-/g, ''),
        "contacts":
          { "email": faker.internet.email() }
      })
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.body).to.have.property('status')
        expect(res.body.status).to.equal(true)
        expect(res.body.data).to.have.property('email')
        personId = res.body.data.id
        done();
      });
  });

})

describe('Get person by Id', function () {
  it('should respond person object', function (done) {
    request(server)
      .get(`/api/person/get/${personId}`)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.body).to.have.property('status')
        expect(res.body.status).to.equal(true)
        expect(res.body.data).to.have.property('id')
        expect(res.body.data.id).to.have.equal(personId)
        done();
      });
  });

})

describe('Update person by Id', function () {
  it('should return updated person object', function (done) {
    request(server)
      .patch(`/api/person/update/${personId}`)
      .send({
        "email": email,
        "name": faker.name.firstName(),
        "surname": faker.name.lastName()
      })
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.body).to.have.property('status')
        expect(res.body.status).to.equal(true)
        expect(res.body.data.email).to.have.equal(email)
        done();
      });
  });

})

describe('GET all person', function () {

  it('should respond with JSON data', function (done) {
    request(server)
      .get('/api/person')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.body).to.have.property('status')
        expect(res.body.status).to.equal(true)
        expect(res.body).to.have.property('data')
        done();
      });
  });
});

describe('Delete person by Id', function () {
  it('should return status true', function (done) {
    request(server)
      .delete(`/api/person/delete/${personId}`)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.body).to.have.property('status')
        expect(res.body.status).to.equal(true)
        done();
      });
  });

})



