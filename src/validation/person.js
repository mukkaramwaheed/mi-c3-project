const { check } = require('express-validator');
const { dateValid, dateLessOrGreater, getAge } = require('../helper/personHelper')
const { constants } = require('../helper/constants')

exports.personValidation = (method) => {
  switch (method) {
    case 'person': {
        return [
          check('email', 'Email is required').not().isEmpty().isEmail().withMessage('Not a valid email').trim().escape(),
          check('name', 'Name is required').not().isEmpty().trim().escape(),
          check('surname', 'Surname is required').not().isEmpty().trim().escape(),
          check('gender', 'Gender must be male or female').not().isEmpty().isIn(['male','female']).optional({ checkFalsy: true }).trim().escape(),
          check('birthday').trim().escape().optional({ checkFalsy: true })
          .custom((birthday, { req }) => {
            if(dateValid(birthday) === false){
                throw new Error(`Date format must be ${constants.DATE_FORMAT}`)
            }
            if(dateLessOrGreater(birthday) == false) {
            throw new Error('Date must be less or equal to the current date')
            }
            return true
           // format yyyy-mm-dd
           // throw new Error('Result ' +  validator.isDate(birthday,[,'-']))
            
          }),
          check('age', 'Age must be numeric').isNumeric().isLength().optional({ checkFalsy: true }).trim().escape()
          .custom((age, {req}) => {
            if(req.body.birthday){
              var birthday = req.body.birthday
              if(parseInt(age) !== parseInt(getAge(birthday))) {
                throw new Error('Invalid age according to birthday date')
              }
            }
            return true
          }),
          check('phone', 'Phone must be numeric').isNumeric().optional({checkFalsy: true}).trim().escape(),
  
        ]
      }
      case 'contacts': {
        return [
          check('contacts.email', 'Not a valid email').isEmail().trim().escape().optional({ checkFalsy: true }),
          check('contacts.name', 'Name is required').optional({checkFalsy: true}).trim().escape(),
          check('contacts.surname', 'Surname is required').optional({checkFalsy: true}).trim().escape(),
          check('contacts.gender', 'Gender must be male or female').optional({checkFalsy: true}).isIn(['male','female']).optional({ checkFalsy: true }).trim().escape(),
          check('contacts.birthday').trim().escape().optional({ checkFalsy: true })
          .custom((birthday, { req }) => {
            if(dateValid(birthday) === false){
                throw new Error(`Date format must be ${constants.DATE_FORMAT}`)
            }
            if(dateLessOrGreater(birthday) == false) {
            throw new Error('Date must be less or equal to the current date')
            }
            return true
           // format yyyy-mm-dd
           // throw new Error('Result ' +  validator.isDate(birthday,[,'-']))
            
          }),
          check('contacts.age', 'Age must be numeric').isNumeric().isLength().optional({ checkFalsy: true }).trim().escape()
          .custom((age, {req}) => {
            if(req.body.contacts.birthday){
              var birthday = req.body.contacts.birthday
              if(parseInt(age) !== parseInt(getAge(birthday))) {
                throw new Error('Invalid age in contacts according to birthday date')
              }
            }
            return true
          }),
          check('contacts.phone', 'Phone must be numeric').isNumeric().optional({checkFalsy: true}).trim().escape(),
  
        ]
      }
  }
}