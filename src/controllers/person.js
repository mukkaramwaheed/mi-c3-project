const logger = require('../../winston-config')
const db = require('../models')
const contact = require('../models/contact')
const Op = require('sequelize').Op;
const { isObject, getPagingData } = require('../helper/personHelper')

  /**
    * Get all person with contacts
    * 
    * @param {Object} req - request object
    * @param {Object} res - responsde object
    *
    * @return {Object} 
 */
  module.exports.getAllPerson = async (req, res) => {
    
    try {

        const limit = req.query.limit ? parseInt(req.query.limit) : 10
        const offset = req.query.page ? 0 + (parseInt(req.query.page)) - 1 * limit : 0

        let result = await db.person.findAndCountAll({
                attributes: { exclude: ['password'] },
                include: [
                { model: db.contact, as: 'contact' }
                ],
                offset: offset,
                limit: limit,
                order: [
                ['email', 'ASC']
                ]
            })
            res.status(200).json({
                status: true,
                data: getPagingData(result, offset, limit)
            })
       
    } catch(err) {

        res.status(500).json({
            status: false,
            message: err.message,
            error: err
        })

    }
    
  }

/**
 * Get person against Id
 * 
 * @param {Object} req - request object
 * @param {Object} res - responsde object
 *
 * @return {Object} 
 */
module.exports.getPersonById = async (req, res) => {

    try {
        let id = req.params.id

        // Check person exist and Id valid
        let personExist = await this.checkPersonExist(+ id)
        if(!personExist) {
            let result = await db.person.findOne({
                where: {
                    id
                },
                include: [{
                    model: db.contact, as: 'contact'
                }]
            })
            res.status(200).json({
                status: true,
                data: result
            })
        } else {
            res.status(400).json({
                status: false,
                message: personExist
            })
        }
      
    } catch(err) {
        res.status(400).json({
            status: false,
            message: err.message
        })
    }
}


/**
 * Create a person
 * 
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @return {Object} 
 */

module.exports.createPerson = async (req, res) => {

    try {


        let data = {
            ...req.body
        }
        // Separate the contact and person object
        let contacts = data.contacts
        let person = data
        delete person["contacts"]

        // Check email exist or not
        logger.info("Check email exist or not")
        let email = person.email
        let emailExist = await db.person.findOne({
            where: {
                email
            }
        })
        if (emailExist) {
            logger.error("Email already exist")
            return res.status(400).json({
                status: false,
                message: 'Email already exist'
            })
        }

        // Check contact object exist
        if (contacts && isObject(contacts) == false) {
            logger.error('Contact must be object')
            return res.status(400).json({
                status: false,
                message: 'Contacts must be object'
            })
        }

        // Create person
        let user = await db.person.create(person)
        logger.info("Save the person")

        if(contacts && isObject(contacts) == true) {
            let personId = { personId: user.id }
            let contactData = { ...contacts, ...personId }
            // Create contacts
            await db.contact.create(contactData)    
        }

        // Send the response
        res.status(200).json({
            status: true,
            data: user
        })
    } catch (err) {
        logger.error(err.message)
        return res.status(400).json({
            status: false,
            message: err.message
        })
    }

}

/**
 * Update person against Id
 * 
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @return {Object} 
 */


module.exports.updatePerson = async (req, res) => {

  
    try {
        let personId = req.params.id
        let email = req.body.email
        let person = await this.checkPersonExist(+ personId)
        if(!person) {

            logger.info('Person found against id')
            let result = await db.person.findAll({
                where: {
                    email: email,
                    id: { [Op.ne]: personId }
                }
            })
    
            if (result.length == 0) {
               logger.info('Update the person')
               await db.person.update({
                    email: req.body.email,
                    name: req.body.name,
                    surname: req.body.surname,
                    phone: req.body.phone,
                    age: req.body.age,
                    gender: req.body.gender,
                    birthday: req.body.birthday
                }, { where: { id: personId } })
    
                let personDetail = await db.person.findOne({
                    where : {
                        id: personId
                    }
                })
        
                res.status(200).json({
                    status: true,
                    data: personDetail
                })
            }
            else {      
                logger.error('Email already exist')
                res.status(200).json({
                    status: true,
                    message: 'Email already exist'
                })
            }
        } else {

            logger.error('Person error')
            res.status(400).json({
                status:false,
                data: person
            })
        }
    } catch (err) {
        logger.error(err.message)
        res.status(400).json({
            status:false,
            message: err.message
        })
    }

}



/**
 * Delete a person against Id
 * 
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @return {Object} 
 */


module.exports.deletePerson = async (req, res) => {

    try {


        let id = req.params.id
        // Check person exist and Id valid
        let personExist = await this.checkPersonExist(+ id)
        if(!personExist) {
        db.person.destroy({
            where: {
                id
            }
        })
        logger.info('Person deleted successfully')
        res.status(200).json({
            status: true,
            message: "Person deleted successfully"
        })

        } else {
            logger.error('Person error')
            res.status(400).json({
                status: false,
                message: personExist
            })
        }
    
    } catch (err) {
        logger.error(err.message)
        return res.status(400).json({
            status: false,
            message: err.message
        })
    }
   
}

/**
 * Check person exist against Id
 * 
 * @param {Object} req - request object
 * @param {Object} res - responsde object
 *
 * @return meg
 */

module.exports.checkPersonExist = async (id) => {

    if (id && Number.isInteger(id)) {
        let result = await db.person.findOne({
            where: {
                id
            }
        })
        if (!result) {
            return 'Person not found'
        }
    } else {

        return 'Invalid Id'
    }
}
