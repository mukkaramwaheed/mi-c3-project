const { validationResult } = require('express-validator')
const logger = require('../../winston-config')

module.exports.validate = (req, res, next) => {
    const errors = validationResult(req)
    if (errors.isEmpty()) {
      return next()
    }
    const extractedErrors = []
    errors.array({ onlyFirstError: true }).map((err) => extractedErrors.push({ [err.param]: err.msg }))
  
    logger.warn(`Validation Error on: '${req.url}'`)
    return res.status(422).json({
      status: false,
      message: 'Validation errors',
      error: extractedErrors
    })
  }
  