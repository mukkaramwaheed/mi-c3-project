const express = require('express')

const router = express.Router()
const personRoutes = require('./persons')

router.use('/person', personRoutes)

module.exports = router
