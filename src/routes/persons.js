const express = require('express')
const router = express.Router()
const { personValidation } = require('../validation/person')
const { validate } = require('../utils/utils')
const { createPerson, deletePerson, getPersonById, updatePerson, getAllPerson } = require('../controllers/person')

router.get('/', getAllPerson)
router.post('/create', [personValidation('person')], [personValidation('contacts')], validate, createPerson)

router.delete('/delete/:id', deletePerson)
router.get('/get/:id', getPersonById)
router.patch('/update/:id', [personValidation('person')],[personValidation('contacts')], validate, updatePerson)

module.exports = router
