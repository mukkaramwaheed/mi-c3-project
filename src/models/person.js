const logger = require('../../winston-config')
module.exports = (sequelize, DataTypes) => {
  const Person = sequelize.define('person', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true
    },
    name: {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    surname: {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    age: {
      type: DataTypes.INTEGER(11),
      defaultValue: '',
      defaultValue: '0'
    },
    gender: {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    birthday: {
      type: DataTypes.STRING(50)
    },
    phone: {
      type: DataTypes.STRING(50),
      defaultValue: ''
    },
    createdAt: DataTypes.DATE,
    modified: DataTypes.DATE
  }
  )

  return Person
}
