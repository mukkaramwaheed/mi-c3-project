const validator = require('validator')
const moment = require('moment')
const { constants } = require('./constants')
const logger = require('../../winston-config')


/**
 * Check type object or not
 * 
 * @param {Object} req - request object
 * @param {Object} res - responsde object
 *
 * @return true or false 
 */
module.exports.isObject = (a) => {
    return (!!a) && (a.constructor === Object);
};


/**
 * Get pagination detail
 * 
 * @param {Object} req - request object
 * @param {Object} res - responsde object
 *
 * @return object
 */

module.exports.getPagingData = (data, page, limit) => {
    const { count, rows } = data
    const currentPage = page ? page * limit : 0
    const totalPages = Math.ceil(count / limit)
  
    return { count, rows, totalPages, currentPage }
}


/**
 * Check date format of date
 * @param {string} dateValue
 * @return boolean
 */

 module.exports.dateValid = (dateValue) => {
    logger.info(`Constant type ${constants.DATE_FORMAT}`)
    return moment(dateValue, constants.DATE_FORMAT,true).isValid()

 }

/**
 * Check date less or greater than the current date
 * @param {string} date
 * @return boolean
 */

module.exports.dateLessOrGreater = (date) => {
    
    var currentDate = moment().format(constants.DATE_FORMAT)
    if(currentDate === date ||  date < currentDate){
        return true
    } else {
        return false
    }
    // return moment().format("YYYY-MM-DD")
    // return !moment(date).isSame(moment(), 'day')
    // return !moment(date).isBefore(moment(), "day")
 }


 module.exports.getAge = (date) => {

    var today = new Date()
    var birthDate = new Date(date)
    var age = today.getFullYear() - birthDate.getFullYear()
    var m = today.getMonth() - birthDate.getMonth()
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
    {
        age--
    }
    return age
 }